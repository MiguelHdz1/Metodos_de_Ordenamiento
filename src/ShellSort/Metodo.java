/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ShellSort;

/**
 *
 * @author mickey
 */
public class Metodo {

    public int[] shell(int A[]) {
        int salto, aux, i;
        boolean cambios;
        for (salto = A.length / 2; salto != 0; salto /= 2) {
            cambios = true;
            while (cambios) { // Mientras se intercambie algún elemento
                cambios = false;
                for (i = salto; i < A.length; i++) // se da una pasada
                {
                    if (A[i - salto] > A[i]) { // y si están desordenados
                        aux = A[i]; // se reordenan
                        A[i] = A[i - salto];
                        A[i - salto] = aux;
                        cambios = true; // y se marca como cambio.
                    }
                }
            }
        }
        return A;
    }
    
    public static void main(String[] args) {
        int A[]={12,12,9,1,90,12,120,19,2};
        
        Metodo m = new Metodo();
        int c[]=m.shell(A);
        
        for (int i = 0; i < c.length; i++) {
            System.out.println(c[i]);
            
        }
        
    }
    
    public String Imprimir(int[] arreglo) {
        String arregloImp = "\t-----------------Ordenamiento 'SELLSORT':-----------------\t\n[";
        for (int i = 0; i < arreglo.length; i++) {
            if (i == arreglo.length - 1) {
                arregloImp += arreglo[i] + "]";
            } else {
                arregloImp += arreglo[i] + ", ";
            }
        }

        return arregloImp;
    }

}
