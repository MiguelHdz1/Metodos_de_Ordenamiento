/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Radix;


import java.util.LinkedList;

/**
 *
 * @author mickey
 */
public class ListaAarreglo {

    public String[] arreglo(LinkedList lista) {
        String[] array = null;
        try {
            array = (String[]) lista.toArray(new String[lista.size()]);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        return array;
    }
    
    public int[] ArregloEntero(String[] array){
        
        int[] Arreglo= new int[array.length];
        try{
        for (int i = 0; i < array.length; i++) {
            Arreglo[i]=Integer.parseInt(array[i]);
            
            
         }
        }
        catch(Exception ex){
            System.out.println(ex);
        }
        
        return Arreglo;
    }


}
