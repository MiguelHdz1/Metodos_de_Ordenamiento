package Burbuja;

public class Metodo {

    public String burbuja(int[] arreglo) {

        boolean romper = false;
        while (true) {
            romper = false;
            for (int i = 1; i < arreglo.length; i++) {

                if (arreglo[i] < arreglo[i - 1]) {

                    int nuevo = arreglo[i];
                    arreglo[i] = arreglo[i - 1];
                    arreglo[i - 1] = nuevo;
                    romper = true;
                }

            }
            if (romper == false) {

                break;

            }

        }
        String arregloImp = "\t-----------------Ordenamiento 'BURBUJA':-----------------\t\n[";
        for (int i = 0; i < arreglo.length; i++) {
            if (i == arreglo.length - 1) {
                arregloImp += arreglo[i] + "]";
            } else {
                arregloImp += arreglo[i] + ", ";
            }
        }

        return arregloImp;

    }

}
