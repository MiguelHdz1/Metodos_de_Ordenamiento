package Estilos;

import java.awt.Color;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JPanel;

public class Fondo extends Thread {

    int r;
    int g;
    boolean b1, b2;
    JPanel panel;

    public Fondo(JPanel panel) {
        super();
        this.panel = panel;

    }

    @Override
    public void run() {
        while (true) {
            try {
                Thread.sleep(17);
            } catch (InterruptedException ex) {
                Logger.getLogger(Fondo.class.getName()).log(Level.SEVERE, null, ex);
            }
            Random color = new Random();
            int c = color.nextInt(255);
            int d = color.nextInt(255);
            if (g >= 0 && g <= 66 && b2 == false) {
                g++;
                if (g == 66) {
                    b2 = true;
                }
            }
            if (g >= 0 && g <= 66 && b2 == true) {
                g--;
                if (g == 0) {
                    b2 = false;
                }
            }
            panel.setBackground(new Color(40, 10, g));

        }
    }

}
