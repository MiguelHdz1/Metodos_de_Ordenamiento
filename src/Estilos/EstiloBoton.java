/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Estilos;

import java.awt.Color;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JButton;

/**
 *
 * @author luismiguel
 */
public class EstiloBoton extends Thread {

    private JButton boton;

    public EstiloBoton(JButton botton) {
        this.boton = botton;

    }

    private Color agregar() {
        int r = (int) (Math.random() * (75) + 0);
        int g = (int) (Math.random() * (0) + 0);
        int b = (int) (Math.random() * (139) +0);
        

        return new Color(r, g, b);
    }

    @Override
    public void run() {

        int a = 0;

        for (a = 1; a < a + 1; a++) {
            boton.setBackground(agregar());
            try {
                Thread.sleep(1200);
            } catch (InterruptedException ex) {
                Logger.getLogger(EstiloBoton.class.getName()).log(Level.SEVERE, null, ex);
            }
        }

    }

}
