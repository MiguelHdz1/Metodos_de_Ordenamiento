package QuickSort;

public class Metodo {

    public int[] QuickSort(int[] numeros, int izq, int der) {

        if (izq >= der) {
            return numeros;
        }
        int i = izq;
        int d = der;

        if (izq != der) {
            int pivote;
            int aux;

            pivote = izq;

            while (izq != der) {
                while (numeros[der] >= numeros[pivote] && izq < der) {
                    der--;
                }
                while (numeros[izq] < numeros[pivote] && izq < der) {
                    izq++;
                }

                if (der != izq) {
                    aux = numeros[der];
                    numeros[der] = numeros[izq];
                    numeros[izq] = aux;
                }
                if (izq == der) {
                    QuickSort(numeros, i, izq - 1);
                    QuickSort(numeros, izq + 1, d);
                }
            }

        } else {
            return numeros;
        }
        return numeros;
    }

    public String Imprimir(int[] arreglo) {
        String arregloImp = "\t-----------------Ordenamiento 'QUICKSORT':-----------------\t\n[";
        for (int i = 0; i < arreglo.length; i++) {
            if (i == arreglo.length - 1) {
                arregloImp += arreglo[i] + "]";
            } else {
                arregloImp += arreglo[i] + ", ";
            }
        }

        return arregloImp;
    }


}
