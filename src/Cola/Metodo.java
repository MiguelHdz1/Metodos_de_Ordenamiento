package Cola;


import java.util.LinkedList;

public class Metodo {

    public LinkedList lista = new LinkedList();

    public boolean Agregar(Object dato) {
        boolean resultado = false;
        try {
            lista.add(dato);
            resultado = true;
        } catch (Exception ex) {
        }
        return resultado;
    }
    
    public boolean Eliminar() {
        boolean resultado = false;
        try {
            lista.removeFirst();
            resultado = true;
        } catch (Exception ex) {
        }
        return resultado;
    }

    public String Mostrar() {
        String listaFinal;
        if (lista.isEmpty()) {
            listaFinal = "La lista de elementos esta vacia";
        } else {
            listaFinal = lista.toString();
        }
        return listaFinal;
    }

    public int ObtenerTamaño(){
        int tamaño = lista.size();
        
        return tamaño;
    }
}
